## Getting Started

First, run the development server:

npm run dev
# or
yarn dev

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Fill in a positive whole number and it will calculate the smallest positive number that is evenly divisible by all of the numbers!

